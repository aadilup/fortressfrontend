export class NISUser {

  birthdate: Date;
  retirementAge: any;
  monthlySalary: any;
  employeeAVCContributions: any;
  investmentStyle: any;
  recentPensionStatementBalance: any;
  pensionStatementDate: Date;

  constructor() {
    this.birthdate = new Date();
    this.pensionStatementDate = new Date();
    this.retirementAge = 67;
    this.monthlySalary = 1500;
    this.employeeAVCContributions = 0.01;
    this.recentPensionStatementBalance = 10000;
    this.investmentStyle = 'AGGRESSIVE';
  }
}

export class NISUserResult {

  annualNISPension: any;
  totalPensionPlanValue: any;
  taxFreeAmountOfRetirement: any;
  amountOfInnova: any;
  estimatedInnovaPensionPayoutFirstYear: any;
  totalEstimatedPensionIncome: any;
  pensionIncomeAsPercentageOfSalary: any;
  lumpsum: any;
  transferOfInnova: any;

  constructor() {
  }
}
