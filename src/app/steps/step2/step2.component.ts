import { Component } from '@angular/core';
import { OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NISUser } from './step2.model';
import { NISUserResult } from './step2.model';
import { NISService } from 'app/steps/nis.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MessageService } from 'primeng/api';

@Component({
  selector: 'app-step2',
  templateUrl: './step2.component.html',
  styleUrls: ['./step2.component.css']
})
export class Step2Component implements OnInit {

  avcContributions: any[] = [0, 0.01, 0.02, 0.03, 0.04, 0.05, 0.06];
  retirementAges: any[] = [60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70];
  investmentStyles: any[] = [
    {
      label: 'Aggressive',
      value: 'AGGRESSIVE'
    },
    {
      label: 'Conservative',
      value: 'CONSERVATIVE'
    }
  ];

  dataModel: NISUser;
  stepNumber = 2;
  result: NISUserResult;
  registerForm: FormGroup;

  constructor(
    private router: Router,
    private nisService: NISService,
    private formBuilder: FormBuilder,
    private messageService: MessageService
  ) {
    this.dataModel = new NISUser();
  }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      birthdate: ['', [Validators.required, Validators.nullValidator]],
      retirementAge: ['', [Validators.required, Validators.min(0), Validators.minLength(1), Validators.maxLength(4)]],
      monthlySalary: ['', [Validators.required, Validators.min(0), Validators.minLength(1), Validators.maxLength(10)]],
      employeeAVCContributions: ['', [Validators.required, Validators.minLength(1), Validators.min(0), Validators.maxLength(10)]],
      investmentStyle: ['', [Validators.required, , Validators.nullValidator]],
      recentPensionStatementBalance: ['', [Validators.required, Validators.min(0), Validators.minLength(1), Validators.maxLength(10)]],
      pensionStatementDate: ['', [Validators.required, Validators.nullValidator]]
    });
  }

  calculate() {
    if (this.registerForm.invalid) {
      return;
    }

    this.nisService.create(this.dataModel).subscribe((httpResponse: any) => {
      if (httpResponse.success === true) {
        this.result = httpResponse.object;
        this.result.amountOfInnova = parseInt(this.result.amountOfInnova, 10);
        this.result.annualNISPension = parseInt(this.result.annualNISPension, 10);
        this.result.estimatedInnovaPensionPayoutFirstYear = parseInt(this.result.estimatedInnovaPensionPayoutFirstYear, 10);
        this.result.pensionIncomeAsPercentageOfSalary = parseInt(this.result.pensionIncomeAsPercentageOfSalary, 10);
        this.result.taxFreeAmountOfRetirement = parseInt(this.result.taxFreeAmountOfRetirement, 10);
        this.result.totalEstimatedPensionIncome = parseInt(this.result.totalEstimatedPensionIncome, 10);
        this.result.totalPensionPlanValue = parseInt(this.result.totalPensionPlanValue, 10);
        this.result.lumpsum = parseInt(this.result.lumpsum, 10);
        this.result.transferOfInnova = parseInt(this.result.transferOfInnova, 10);

        this.changeToNextStep();
      } else {
        this.messageService.add({ severity: 'error', summary: 'Failure', detail: 'Incorrect details please enter valid details' });
      }
    }, (error) => {
      this.messageService.add({ severity: 'error', summary: 'Failure', detail: 'Incorrect details please enter valid details' });
    });
  }

  changeToNextStep() {
    if (this.stepNumber < 7) {
      this.stepNumber += 1;
    }
  }

  changeToPreviousStep() {
    if (this.stepNumber > 2) {
      this.stepNumber -= 1;
    }
  }

  // convenience getter for easy access to form fields
  get f() {
    return this.registerForm.controls;
  }

}
