import { Component } from '@angular/core';
import { OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NISUserB } from './step3CalcB.model';
import { NISUserBResult } from './step3CalcB.model';
import { NISCalcBService } from 'app/steps/nisCalcB.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MessageService } from 'primeng/api';

@Component({
  selector: 'app-step3calcb',
  templateUrl: './step3CalcB.component.html',
  styleUrls: ['./step3CalcB.component.css']
})
export class Step3CalcBComponent implements OnInit {

  stepNumber = 1;
  dataModel: NISUserB;
  result: NISUserBResult;
  registerForm: FormGroup;

  interestRates: any[] = [0.03, 0.05, 0.07];
  retirementAges: any[] = [60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70];

  constructor(
    private router: Router,
    private nisCalcBService: NISCalcBService,
    private formBuilder: FormBuilder,
    private messageService: MessageService
  ) {
    this.dataModel = new NISUserB();
  }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      birthdate: ['', [Validators.required, Validators.nullValidator]],
      retirementAge: ['', [Validators.required, Validators.min(0), Validators.minLength(1), Validators.maxLength(4)]],
      currentSavingsInPension: ['', [Validators.required, Validators.min(0), Validators.minLength(1), Validators.maxLength(10)]],
      interestRate: ['', [Validators.required, Validators.minLength(1), Validators.min(0), Validators.maxLength(10)]],
      monthlySavings: ['', [Validators.required, Validators.min(0), Validators.minLength(1), Validators.maxLength(10)]]
    });
  }

  calculate() {
    if (this.registerForm.invalid) {
      return;
    }

    this.nisCalcBService.create(this.dataModel).subscribe((httpResponse: any) => {

      if ( httpResponse.success === true ) {
        this.result = httpResponse.object;
        this.changeToNextStep();
      } else {
        this.messageService.add({ severity: 'error', summary: 'Failure', detail: 'Incorrect details please enter valid details' });
      }
    }, (error) => {
      this.messageService.add({ severity: 'error', summary: 'Failure', detail: 'Incorrect details please enter valid details' });
    });
  }

  changeToNextStep() {
    if (this.stepNumber < 2) {
      this.stepNumber += 1;
    }
  }

  changeToPreviousStep() {
    if (this.stepNumber > 1) {
      this.stepNumber -= 1;
    }
  }

  // convenience getter for easy access to form fields
  get f() {
    return this.registerForm.controls;
  }

}
