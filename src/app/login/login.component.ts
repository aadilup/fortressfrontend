import { Component } from '@angular/core';
import { OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoginModel } from './login.model';
import { SessionHelper } from 'app/core/session.helper';
import { LoginService } from './login.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MessageService } from 'primeng/api';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  data: LoginModel;
  registerForm: FormGroup;

  constructor(
    private router: Router,
    private sessionHelper: SessionHelper,
    private loginService: LoginService,
    private formBuilder: FormBuilder,
    private messageService: MessageService
  ) {


    this.data = new LoginModel();
    if (this.sessionHelper.getUser() !== null && this.sessionHelper.getUser() !== null) {
      this.router.navigate(['/dashboard']);
    }
  }

  login() {
    // stop here if form is invalid
    if (this.registerForm.invalid) {
      return;
    }

    this.loginService.create(this.data).subscribe((httpResponse: any) => {

      const response = httpResponse;

      if (response.success === true) {
        this.sessionHelper.setUser(response.object);

        const routerParams = null;
        this.messageService.add({ severity: 'success', summary: 'Loggedin', detail: 'Login successfully' });
        this.router.navigate(['/dashboard']);

      } else {
        this.messageService.add({ severity: 'error', summary: 'Failure', detail: 'Username or Password is incorrect' });
        this.sessionHelper.setUser(null);
      }

    },
      err => {
        console.log(err);
        this.messageService.add({ severity: 'error', summary: 'Failure', detail: 'Username or Password is incorrect' });
        this.sessionHelper.setUser(null);
      }
    );
  }

  // convenience getter for easy access to form fields
  get f() { return this.registerForm.controls; }


  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      username: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]]
    });
  }

}
