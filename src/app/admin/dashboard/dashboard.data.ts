export class UiDashboard {

     regularContributionBelowCeiling: any;
     regularContributionAboveCeiling: any;
     avcContributionBelowCeiling: any;
     avcContributionAboveCeiling: any;
     nisCeiling: any;
     mandatoryRetirementAge: any;
     assumedWorkingAge: any;
     aggressive: any;
     conservative: any;

     constructor () {

       this.regularContributionBelowCeiling = 0;
       this.regularContributionAboveCeiling = 0;
       this.avcContributionBelowCeiling = 0;
       this.avcContributionAboveCeiling = 0;
       this.nisCeiling = 0;
       this.mandatoryRetirementAge = 0;
       this.assumedWorkingAge = 0;
       this.aggressive = 0;
       this.conservative = 0;

     }
}
