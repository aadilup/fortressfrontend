import { Injectable } from '@angular/core';
import { LocalStorageService } from 'angular-2-local-storage';
import { CurrentUser } from 'app/model/model';

@Injectable()
export class SessionHelper {

  constructor(
    private storage: LocalStorageService
  ) {
  }

  set(key: string, value: any) {
    this.storage.set(key, value);
  }

  get<T>(key: string): T {
    const item = this.storage.get(key);
    if (!item || item === 'undefined' || item === null) {
      return null;
    }
    return <T>item;
  }

  getUser(): CurrentUser {
    return this.get<CurrentUser>('user');
  }

  setUser(user: CurrentUser) {
    if (user == null) {
      this.storage.set('user', null);
    } else {
      this.storage.set('user', user);
    }
  }

  hasPermission(permission: String) {
    let hasPermission = false;
    const userPermissions = this.getUser().authorities;
    if (userPermissions.filter(pri => pri.authority.toUpperCase() === permission.toUpperCase()).length > 0) {
      hasPermission = true;
    }

    return hasPermission;
  }

  hasAnyPermission(permissions: Array<String>) {
    let hasPermission = false;
    const userPermissions = this.getUser().authorities;

    for (let i = 0; i < permissions.length; i++) {

      const permission = permissions[i];
      if (userPermissions.filter(pri => pri.authority.toUpperCase() === permission.toUpperCase()).length > 0) {
        hasPermission = true;
        break;
      }
    }

    return hasPermission;
  }

  hasAllPermission(permissions: Array<String>) {
    let hasPermission = false;
    const userPermissions = this.getUser().authorities;

    for (let i = 0; i < permissions.length; i++) {

      const permission = permissions[i];

      if (userPermissions.filter(pri => pri.authority.toUpperCase() === permission.toUpperCase()).length > 0) {
        hasPermission = true;
      } else {
        hasPermission = false;
        break;
      }
    }

    return hasPermission;
  }

  removeAll() {
    this.storage.clearAll();
  }

  remove(key: string) {
    this.storage.remove(key);
  }
}
