import { Injectable } from '@angular/core';
import { SessionHelper } from '../core/session.helper';
import { LocalStorageService } from 'angular-2-local-storage';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';

@Injectable()
export class GlobalService {

  LOADER_COUNTER: number;
  API_END_POINT: string;
  SESSION_TIMEOUT: any;
  SESSION_TIMEOUT_TIMER: any;

  constructor(
    private storage: LocalStorageService,
    private sessionHelper: SessionHelper,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {

    this.LOADER_COUNTER = 0;
    this.SESSION_TIMEOUT = 15;      // In Minutes
    this.SESSION_TIMEOUT_TIMER = 30;   // In Seconds

    // API END POINT FOR LOCAL MACHINE
    // this.API_END_POINT = 'http://localhost:8080/PROJECT-NAME/';

    // API END POINT FOR AWS
    // this.API_END_POINT = 'http://localhost:8080/fortress-0.0.1/';

    // API END POINT FOR AWS PRODUCTION
    this.API_END_POINT = 'http://localhost:8080/Fortress/';

    // activatedRoute.url.subscribe(
    //   url => {
    //
    //   }
    // );

  }

  getCounter() {
    return this.LOADER_COUNTER;
  }

  incrementCounter() {
    this.LOADER_COUNTER++;
  }

  decrementCounter() {
    if (this.LOADER_COUNTER > 0) {
      this.LOADER_COUNTER--;
    }
  }

}
