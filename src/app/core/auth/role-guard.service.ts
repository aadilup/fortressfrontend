import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot } from '@angular/router';
import { SessionHelper } from 'app/core/session.helper';

@Injectable()
export class RoleGuardService implements CanActivate {

  constructor(private sessionHelper: SessionHelper,
    public router: Router) { }

  canActivate(route: ActivatedRouteSnapshot): boolean {

    const expectedRole: String = route.data.expectedRole;

    if (this.sessionHelper.getUser() !== null && this.sessionHelper.getUser() !== undefined) {
      const authList = this.sessionHelper.getUser().authorities;

      for (let i = 0; i < authList.length; i++) {

        if (authList[i].authority === String(expectedRole)) {
          return true;
        }
      }
      // this.router.navigate(['login']);
    }
    return false;
  }
}
