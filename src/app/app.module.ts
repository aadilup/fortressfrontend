import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HashLocationStrategy } from '@angular/common';
import { LocationStrategy } from '@angular/common';

// COMPONENT
import { AppComponent } from './app.component';
import { HomeComponent } from 'app/home/home.component';
import { FooterComponent } from 'app/shared/footer/footer.component';
import { HeaderComponent } from 'app/shared/header/header.component';
import { WelcomeComponent } from 'app/welcome/welcome.component';
import { MainComponent } from 'app/main/main.component';
import { Step1Component } from 'app/steps/step1/step1.component';
import { Step2Component } from 'app/steps/step2/step2.component';
import { Step3Component } from 'app/steps/step3/step3.component';
import { Step3CalcAComponent } from 'app/steps/step3CalcA/step3CalcA.component';
import { Step3CalcBComponent } from 'app/steps/step3CalcB/step3CalcB.component';
import { LoginComponent } from 'app/login/login.component';
import { DashboardComponent } from 'app/admin/dashboard/dashboard.component';
import { ForgotPasswordComponent } from 'app/forgot-password/forgot-password.component';

// MODULES
import { AppRouteProvider } from 'app/app.routes';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { FormsModule } from '@angular/forms';
import { LocalStorageModule } from 'angular-2-local-storage';
import { ToastModule } from 'primeng/toast';

// SERVICES
import { GlobalService } from 'app/core/global';
import { SessionHelper } from 'app/core/session.helper';
import { HttpRestClient } from 'app/core/http.client';
import { NISService } from 'app/steps/nis.service';
import { NISCalcBService } from 'app/steps/nisCalcB.service';
import { NISCalcAService } from 'app/steps/nisCalcA.service';
import { LoginService } from 'app/login/login.service';
import { RoleGuardService } from 'app/core/auth/role-guard.service';
import { ForgotPasswordService } from 'app/forgot-password/forgot-password.service';
import { MessageService } from 'primeng/api';

// DIRECTIVES
import { NumberFormatFilter } from 'app/shared/directives/numberFormatFilter.pipe';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    FooterComponent,
    HeaderComponent,
    WelcomeComponent,
    MainComponent,
    Step1Component,
    Step2Component,
    Step3Component,
    Step3CalcAComponent,
    Step3CalcBComponent,
    LoginComponent,
    DashboardComponent,
    ForgotPasswordComponent,
    NumberFormatFilter
  ],
  imports: [
    BrowserModule,
    AppRouteProvider,
    HttpClientModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    ToastModule,
    BsDatepickerModule.forRoot(),
    LocalStorageModule.withConfig({
      prefix: 'fortress',
      storageType: 'localStorage'
    })
  ],
  providers: [
    { provide: LocationStrategy, useClass: HashLocationStrategy },
    GlobalService,
    SessionHelper,
    HttpRestClient,
    NISService,
    NISCalcBService,
    NISCalcAService,
    LoginService,
    RoleGuardService,
    ForgotPasswordService,
    MessageService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
