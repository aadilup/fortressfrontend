import { FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { RouterModule } from '@angular/router';
import { Routes } from '@angular/router';

// COMPONENTS
import { HomeComponent } from 'app/home/home.component';
import { WelcomeComponent } from 'app/welcome/welcome.component';
import { MainComponent } from 'app/main/main.component';
import { Step1Component } from 'app/steps/step1/step1.component';
import { Step2Component } from 'app/steps/step2/step2.component';
import { Step3Component } from 'app/steps/step3/step3.component';
import { Step3CalcAComponent } from 'app/steps/step3CalcA/step3CalcA.component';
import { Step3CalcBComponent } from 'app/steps/step3CalcB/step3CalcB.component';
import { LoginComponent } from 'app/login/login.component';
import { DashboardComponent } from 'app/admin/dashboard/dashboard.component';
import { RoleGuardService } from 'app/core/auth/role-guard.service';
import { ForgotPasswordComponent } from 'app/forgot-password/forgot-password.component';

const AppRoutes: Routes = [
  {
    path: '',
    component: HomeComponent,
    children: [
      {
        path: '',
        redirectTo: 'welcome',
        pathMatch: 'full'
      },
      {
        path: 'welcome',
        component: WelcomeComponent
      },
      {
        path: 'login',
        component: LoginComponent
      },
      {
        path: 'main',
        component: MainComponent,
        children: [
          {
            path: 'step1',
            component: Step1Component
          },
          {
            path: 'step2',
            component: Step2Component
          },
          {
            path: 'step3',
            component: Step3Component
          },
          {
            path: 'step3calca',
            component: Step3CalcAComponent
          },
          {
            path: 'step3calcb',
            component: Step3CalcBComponent
          }
        ]
      },
      {
        path: 'forgot-password',
        component: ForgotPasswordComponent
      }
    ]
  },
  {
    path: 'home',
    component: HomeComponent
  },
  {
    path: 'dashboard',
    component: DashboardComponent,
    canActivate: [RoleGuardService],
    data: {
      expectedRole: "DASHBOARD_READ_WRITE"
    }
  }
];

export const AppRouteProvider = RouterModule.forRoot(AppRoutes);
