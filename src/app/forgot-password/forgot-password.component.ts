import { Component } from '@angular/core';
import { OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ForgotPasswordService } from 'app/forgot-password/forgot-password.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent implements OnInit {

  formId = 'RESET';
  registerForm: FormGroup;
  registerForm2: FormGroup;

  constructor(
    private router: Router,
    private forgotPasswordService: ForgotPasswordService,
    private formBuilder: FormBuilder
  ) {
  }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      username: ['', [Validators.required, Validators.email]]
    });

    this.registerForm2 = this.formBuilder.group({
      password: ['', [Validators.required, Validators.minLength(6)]]
    });
  }

  resetPasswordRequest() {
    // stop here if form is invalid
       if (this.registerForm.invalid) {
           return;
       }

     this.formId = 'VERIFY';
  }

  validateOTP() {
    // stop here if form is invalid
       if (this.registerForm2.invalid) {
           return;
       }

  }

  // convenience getter for easy access to form fields
  get f() { return this.registerForm.controls; }

  // convenience getter for easy access to form fields
  get f2() { return this.registerForm2.controls; }

}
